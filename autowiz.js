/**
 * BREADCRUMB HISTORY
 */
class Breadcrumbs {
    constructor() {
        this.states = [];
        this.initialized = false;
    }

    addState(name) {
        this.states.push(name);
    }

    popState() {
        this.states.pop();
    }

    render() {
        if (this.initialized === false) {
            // target for breadcrumbs
            const target = document.querySelector('.wz-bc-target');

            // create list
            const breadcrumbList = document.createElement('ul');
            breadcrumbList.setAttribute('id', 'wz-breadcrumbs');

            // add list to DOM below target
            target.insertAdjacentElement('afterbegin', breadcrumbList);

            this.initialized = true;
        }

        const parent = document.querySelector('#wz-breadcrumbs');
        const nodeList = parent.querySelectorAll('li');

        // remove all children
        if (parent.childElementCount > 0) {
            nodeList.forEach(element => {
                parent.removeChild(element);
            });
        }

        // add children
        this.states.forEach(state => {
            const node = document.createElement("li");
            node.style.cssFloat = 'left';
            node.style.marginRight = '15px';
            const textnode = document.createTextNode(state);
            node.appendChild(textnode);
            parent.appendChild(node);
        });
    }

    addBreadcrumb(text) {
        const node = document.createElement("li");
        const textnode = document.createTextNode(text);
        node.appendChild(textnode);
        document.getElementById("wz-breadcrumbs").appendChild(node);
    }
}

/**
 * FIELDSET WIZARD
 * 
 * Automatically create a wizard from a set of fieldsets.
 */

class FieldsetWizard {
    constructor(options = null) {
        this.wizardFieldsets = null;
        this.wizardMap = {};
        this.currentPosition = 0;

        if (options != null) {
            this.options = options;
        }
    }

    init() {
        this.getFieldsets();
        this.createMap();
        this.setDisplayMode('hide', this.wizardFieldsets);
        this.configureOptions();
    }

    configureOptions() {
        if (!'options' in this) {
            console.log('wz-warning: no options to configure');
            return;
        }

        if (this.options.breadcrumbs === true) {
            this.breadcrumbs = new Breadcrumbs();
        }
    }

    unlockedCheck(fieldsetId) {
        const inputs = this.wizardMap[fieldsetId].inputs;

        let status = true;

        Object.entries(inputs).forEach(([key, value]) => {
            if (value === 'locked') {
                status = false;
            }
        })

        return status;
    }

    render(mode) {
        const id = this.wizardFieldsets[this.currentPosition].id;
        this.unlockedCheck(id);

        if (mode == 'next' && !this.unlockedCheck(id)) {
            console.log('wz-warning: fields still locked');
            return;
        }

        switch (mode) {
            case 'start':
                this.setDisplayMode('hide', this.wizardFieldsets);
                this.setDisplayMode('show', this.wizardFieldsets[0]);
                this.addButtons();

                if (this.options.breadcrumbs === true) {
                    this.breadcrumbs.addState(this.wizardFieldsets[this.currentPosition].querySelector('fieldset legend span.x-fieldset-header-text').textContent);
                }
                break;
            case 'prev':
                this.setDisplayMode('hide', this.wizardFieldsets[this.currentPosition]);
                this.setDisplayMode('show', this.wizardFieldsets[this.currentPosition - 1]);
                this.currentPosition--;
                this.addButtons();

                if (this.options.breadcrumbs === true) {
                    this.breadcrumbs.popState();
                }
                break;
            case 'next':
                this.setDisplayMode('hide', this.wizardFieldsets[this.currentPosition]);
                this.setDisplayMode('show', this.wizardFieldsets[this.currentPosition + 1]);
                this.currentPosition++;
                this.addButtons();

                if (this.options.breadcrumbs === true) {
                    this.breadcrumbs.addState(this.wizardFieldsets[this.currentPosition].querySelector('fieldset legend span.x-fieldset-header-text').textContent);
                }
                break;
            default:
                console.log('wz-error: illegal render mode argument');
        }

        if (this.options.breadcrumbs === true) {
            this.breadcrumbs.render();
        }
    }

    addButtons() {
        const currentFieldsetName = this.wizardFieldsets[this.currentPosition].querySelector('fieldset legend span.x-fieldset-header-text').textContent;

        // button target
        const target = document.querySelector('.wz-buttons-target');

        // create container
        const container = document.createElement('div');
        container.setAttribute('id', 'wz-button-container');
        container.style.cssFloat = "right";
        //container.appendChild(document.createTextNode('some text'));

        // add container to DOM after target
        target.insertAdjacentElement('afterend', container);

        // remove current buttons
        const currentButtons = document.querySelectorAll('input.wz-button');

        if (currentButtons.length > 0) {
            currentButtons.forEach(element => {
                element.parentNode.removeChild(element);
            });
        }

        // create prev button
        if ('prevButtons' in this.options) {
            const hasButton = this.options.prevButtons.includes(currentFieldsetName);

            if (hasButton) {
                const button = document.createElement("input");
                button.innerHTML = 'prev';
                button.setAttribute("onclick", "wizard.render('prev')");
                button.setAttribute("title", "prev");
                button.setAttribute("value", "prev");
                button.setAttribute("type", "button");
                button.classList.add("wz-button", "buttons");
                container.appendChild(button);
            }
        }

        // create next button
        if ('nextButtons' in this.options) {
            const hasButton = this.options.nextButtons.includes(currentFieldsetName);

            if (hasButton) {
                const button = document.createElement("input");
                button.innerHTML = 'next';
                button.setAttribute("onclick", "wizard.render('next')");
                button.setAttribute("title", "next");
                button.setAttribute("value", "next");
                button.setAttribute("type", "button");
                button.classList.add("wz-button", "buttons");
                container.appendChild(button);
            }
        }
    }

    getFieldsets() {
        this.wizardFieldsets = document.querySelectorAll("form fieldset");
    }

    createMap() {
        this.wizardFieldsets.forEach(element => {
            // add fieldset id to object
            const fieldsetId = element.id;
            this.wizardMap[fieldsetId] = {};

            // add fieldset legend text to object
            const legendText = element.querySelector("fieldset > legend").textContent;
            this.wizardMap[fieldsetId]['legend'] = legendText;

            // add next/prev buttons
            this.wizardMap[fieldsetId]['btnNext'] = false;
            this.wizardMap[fieldsetId]['btnPrev'] = false;

            // add inputs object
            this.wizardMap[fieldsetId]['inputs'] = {};

            // add locked input fields to object
            const fieldNodes = element.querySelectorAll("fieldset > input.wz-locked");
            fieldNodes.forEach(element => {
                const fieldId = element.id;
                this.wizardMap[fieldsetId]['inputs'][fieldId] = 'locked';
            });
        });
    }

    unlockField(fieldId) {
        for (var fieldset in this.wizardMap) {
            for (var prop in this.wizardMap[fieldset]['inputs']) {
                if (prop === fieldId) {
                    this.wizardMap[fieldset]['inputs'][prop] = 'unlocked';
                }
            }
        }
    }

    lockField(fieldId) {
        console.log('locking ' + fieldId);
        for (var fieldset in this.wizardMap) {
            for (var prop in this.wizardMap[fieldset]['inputs']) {
                if (prop === fieldId) {
                    this.wizardMap[fieldset]['inputs'][prop] = 'locked';
                }
            }
        }
    }

    setDisplayMode(mode, domObject) {
        switch (mode) {
            case 'show':
                if (domObject instanceof Element) {
                    domObject.style.display = 'block';
                } else if (domObject instanceof NodeList) {
                    domObject.forEach(function (node) {
                        node.style.display = 'block';
                    });
                } else {
                    console.log('dom is neither Element or NodeList');
                    break;
                }
                break;

            case 'hide':
                if (domObject instanceof Element) {
                    domObject.style.display = 'none';
                } else if (domObject instanceof NodeList) {
                    domObject.forEach(function (node) {
                        node.style.display = 'none';
                    });
                } else {
                    console.log('dom is neither Element or NodeList');
                    break;
                }
                break;
            default:
                console.log('setElemDisplayMode error: faulty mode argument')
        }
    }
}