# AutoWiz
Generate a series of wizard-like steps to complete a html form that is based on fieldsets siblings.
## Features
- Auto-generated completion steps
- Auto-generated breadcrumb history (optional)
- Auto-generated prev/next buttons
- Skip-ahead prevention
## Usage

**Fieldset**

All fieldsets that are children to a form will automatically be included in the wizard. The fieldset wizard ordering will correspond to the placement of the fieldset in the DOM.

**Breadcrumb history**

Breadcrumbs will be extracted from the fieldset legend automatically. For this, we need an element with the class **wz-bc-target** to target the location where the breadcrumb list will be created. The list will be created as the first sibling of the target.

**Field validation**

All fields in the fieldset that will require some form of validation on your behalf needs to have a class of wz-locked.
```
<input id="firstName" type="text" class="wz-locked" onkeyup="yourValidation(this)">
```
After your validation logic has executed, you need to call the wizards lock or unlock function from your function and pass the target element id as an argument to lock/unlock the field .

**_Note: All fields with class wz-locked must be unlocked to advance the wizard._**
```javascript
yourValidation = function (element) {

    // example unlock logic
    if (element.value.length > 0) {
        wizard.unlockField(element.id);
    }

    // example lock logic
    if (element.value.length < 1) {
        wizard.lockField(element.id);
    }
}
```

**Navigation buttons**

Navigation buttons is automatically created. Just as with the breadcrumb list, we need a target element where the buttons will be created. This is an element with the class of **wz-buttons-target**. A div containing the buttons will be created as the targets first sibling. Declare fieldset buttons in the options object, otherwise no buttons will appear.

**Options**

Pass an object with options to the wizard constructor to define:
- Breadcrumbs enable/disable
- Prev and Next buttons for each fieldset. Specify the fieldset label to target the correct fieldset.

```javascript
const options = {
    breadcrumbs: true,
    prevButtons: ['fieldset2', 'fieldset3', 'fieldset4'],
    nextButtons: ['fieldset1', 'fieldset2', 'fieldset3']
}
```

**Create Wizard**

To create the wizard, simply instantiate the FieldsetWizard class with the options object, call init and render.

```javascript
const wizard = new FieldsetWizard(options);
wizard.init();
wizard.render('start');
```













